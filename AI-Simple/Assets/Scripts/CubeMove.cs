using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CubeMove : MonoBehaviour
{

    //VARIABLES
    public GameObject targetObject1;
    public GameObject targetObject2;
    public float idleDistance = 0.5f;

    private NavMeshAgent agent;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        targetObject1.SetActive(true);
        targetObject2.SetActive(false);
    }

    // Update is called once per frame
    void tarUpdate(GameObject target, GameObject target2, bool state)
    {
        agent.SetDestination(target.transform.position);
        if (Vector3.Distance(transform.position, target.transform.position) > idleDistance)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
            target.SetActive(state);
            target2.SetActive(!state);
        }
    }

    void Update()
    {

        if (targetObject1.activeSelf == true)
        {
            tarUpdate(targetObject1, targetObject2, false);
        }
        else
        {
            if(targetObject2.activeSelf == true)
            {
                tarUpdate(targetObject2, targetObject1, false);
            }
        }
    }
}
