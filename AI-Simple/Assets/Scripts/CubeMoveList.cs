using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CubeMoveList : MonoBehaviour
{

    //VARIABLES
    public List<GameObject> targetObjects;
    private GameObject currentTarget;

    private NavMeshAgent agent;
    private Animator animator;

    public float idleDistance = 0.6f;
    private int targetCounter = 0;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        currentTarget = targetObjects[targetCounter];
    }

    void Update()
    {
        agent.SetDestination(currentTarget.transform.position);
        if (Vector3.Distance(transform.position, currentTarget.transform.position) > idleDistance)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
            targetCounter++;
            if (targetCounter >= targetObjects.Count)
            {
                targetCounter = 0;
            }
            currentTarget = targetObjects[targetCounter];
        }
    }
}
